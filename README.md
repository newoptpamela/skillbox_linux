# Задание 1. Установка sshd на Ubuntu в Docker контейнере(Для ВМ все аналогично)

## Шаги выполнения

1. **Запуск контейнера с Ubuntu и установкой SSH:**

    ```bash
    docker run --privileged -d -p 2222:22 --name ubuntu-sshd ubuntu:latest
    ```

2. **Подключение к контейнеру:**

    ```bash
    docker exec -it ubuntu-sshd bash
    ```

3. **Установка необходимых пакетов:**

    ```bash
    apt update
    apt install -y openssh-server iproute2 iptables
    ```

4. **Запуск SSH-сервера:**

    ```bash
    service ssh start
    ```

5. **Проверка работы SSH:**

    ```bash
    ss -ntlp | grep ssh
    ```

   Результат:
    ```text
    LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=1,fd=3))
    LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=1,fd=4))
    ```

6. **Получение IP-адреса контейнера:**

    ```bash
    ip ad
    ```

   Результат:
    ```text
    26: eth0@if27: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65535 qdisc noqueue state UP group default 
        link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
           valid_lft forever preferred_lft forever
    ```

7. **Проверка правил iptables:**

    ```bash
    iptables -L
    ```

   Результат:
    ```text
    Chain INPUT (policy ACCEPT)
    target     prot opt source               destination         

    Chain FORWARD (policy ACCEPT)
    target     prot opt source               destination         

    Chain OUTPUT (policy ACCEPT)
    target     prot opt source               destination
    ```

## Скриншоты

- ![Команда 'ip ad'](Задание%201.%20Установка%20sshd%20на%20Ubuntu/Команда%20'ip%20ad'.png)
- ![Команда 'iptables -L'](Задание%201.%20Установка%20sshd%20на%20Ubuntu/Команда%20'iptables%20-L'.png)
- ![Команда 'ss -ntlp | grep ssh'](Задание%201.%20Установка%20sshd%20на%20Ubuntu/Команда%20'ss%20-ntlp%20%7C%20grep%20ssh'.png)

# Задание 2. DROP по умолчанию в цепочке INPUT

## Шаги выполнения

1. **Настройка политики по умолчанию DROP для цепочки INPUT**:

    ```bash
    docker exec -it ubuntu-sshd bash
    iptables -P INPUT DROP
    iptables -A INPUT -s 192.168.0.0/16 -j ACCEPT
    iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -L
    ```

2. **Проверка соединения с сайтом**:

    ```bash
    apt update
    apt install -y curl
    curl -I www.google.com
    ```

3. **Ответ на вопрос**:

    
    "Пользователь не сможет посетить сайты, такие как www.google.com, при политике DROP по умолчанию для цепочки INPUT. Это связано с тем, что входящие ответы на исходящие запросы будут блокироваться. Чтобы решить эту проблему, нужно разрешить входящие пакеты для установленных соединений и связанных соединений.


## Скриншоты

- ![Команда `iptables -L`](Задание%202%20DROP%20по%20умолчанию%20в%20цепочке%20INPUT/iptables_list.png)
- ![Команда `curl -I www.google.com`](Задание%202%20DROP%20по%20умолчанию%20в%20цепочке%20INPUT/curl_google.png)

## Задание 3. ACCEPT по умолчанию в цепочке OUTPUT

### Шаги выполнения:

1. Установка политики DROP для INPUT и ACCEPT для OUTPUT:
    ```bash
    iptables -P INPUT DROP
    iptables -P OUTPUT ACCEPT
    iptables -A INPUT -p tcp --dport 80 -j ACCEPT
    iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    ```
2. Проверка соединения с Mail.ru:
    ```bash
    curl -I http://mail.ru
    ```

### Ответ на вопрос:
"Пользователь не сможет посетить сайт mail.ru, потому что политика DROP по умолчанию для цепочки INPUT блокирует входящие пакеты, не относящиеся к HTTP на порту 80. Чтобы решить эту проблему, необходимо разрешить входящие пакеты для соответствующих соединений."

## Скриншоты

- ![Команда `iptables -L`](Задание%203.%20ACCEPT%20по%20умолчанию%20в%20цепочке%20OUTPUT/Команда_iptables-L.png)
- ![Команда `curl -I http://mail.ru`](Задание%203.%20ACCEPT%20по%20умолчанию%20в%20цепочке%20OUTPUT/Команда_curl-I_httpmail.ru.png)


## Задание 4. ACCEPT для трафика SSH

### Шаги выполнения:

1. Разрешение SSH-трафика:
    ```bash
    iptables -A INPUT -p tcp --dport 22 -j ACCEPT
    ```
2. Сохранение правил iptables:
    ```bash
    apt install -y iptables-persistent
    netfilter-persistent save
    ```
3. Установка и настройка SSH:
    ```bash
    apt update
    apt install -y openssh-server
    service ssh start
    ```
4. Подключение к контейнеру через SSH:
    ```bash
    ssh root@localhost -p 2222
    ```

### Ответ на вопрос:
"Пользователь сможет подключиться к контейнеру через SSH, поскольку разрешено входящее соединение на порт 22. Правила iptables настроены правильно, и SSH-сервер работает."

## Скриншоты

- ![Сохранение правил iptables](Задание%204.%20ACCEPT%20для%20трафика%20SSH/Задание_4_1.png)
- ![Установка пароля](Задание%204.%20ACCEPT%20для%20трафика%20SSH/Задание_4_2.png)
- ![Подключение через SSH](Задание%204.%20ACCEPT%20для%20трафика%20SSH/Задание_4_3.png)
- ![Команда `iptables -L`](Задание%204.%20ACCEPT%20для%20трафика%20SSH/Команда_iptables-L.png)


## Задание 5. ACCEPT/DROP для цепочек INPUT/OUTPUT

### Шаги выполнения:

1. Установка политики DROP для INPUT и ACCEPT для OUTPUT:
    ```bash
    iptables -P INPUT DROP
    iptables -P OUTPUT ACCEPT
    ```
2. Разрешение сеансов, исходящих от виртуальной машины 1 (для уже установленных соединений):
    ```bash
    iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    ```

### Проверка:

1. Проверка исходящих соединений:
    ```bash
    ping google.com
    ```
2. Проверка правил iptables:
    ```bash
    iptables -L
    ```

### Ответ на вопрос:
"Этот набор правил и политик работает, потому что политика INPUT DROP блокирует все входящие соединения по умолчанию, а политика OUTPUT ACCEPT разрешает все исходящие соединения по умолчанию. Правило для ESTABLISHED,RELATED в цепочке INPUT разрешает входящие пакеты, относящиеся к уже установленным соединениям, таким как ответы на исходящие запросы, что обеспечивает успешный обмен пакетами с виртуальной машины 1."

## Скриншоты

- ![Установка политики DROP для INPUT и ACCEPT для OUTPUT](Задание%205.%20ACCEPT-DROP%20для%20цепочек%20INPUT-OUTPUT/Задание_4_1.png)
- ![Команда `iptables -L`](Задание%205.%20ACCEPT-DROP%20для%20цепочек%20INPUT-OUTPUT/Задание_4_2.png)
- ![Проверка исходящих соединений](Задание%205.%20ACCEPT-DROP%20для%20цепочек%20INPUT-OUTPUT/Команда_iptables-L.png)


## Задание 6. DROP INPUT & OUTPUT

### Шаги выполнения:

1. Установите политику по умолчанию для цепочек INPUT и OUTPUT - DROP:
    ```bash
    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    ```

2. Разрешите только удалённым машинам пинговать локальную машину и заблокируйте локальной машине возможность пинга других машин:
    ```bash
    iptables -F
    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    iptables -A OUTPUT -p udp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A INPUT -p udp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A INPUT -p tcp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
    iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
    ```

3. Проверка:
   - Локальная машина (`ubuntu1`):
     ```bash
     ping google.com  # Пинг не должен работать
     ping 172.17.0.2  # Пинг должен быть разрешён
     ```

   - Удалённая машина (`ubuntu2`):
     ```bash
     ping 172.17.0.2  # Пинг должен быть разрешён
     ```

4. Разрешите пинговать удалённые компьютеры только локальному компьютеру и заблокируйте пинг от удалённых компьютеров на локальном компьютере:
    ```bash
    iptables -F
    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    iptables -A OUTPUT -p udp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A INPUT -p udp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A INPUT -p tcp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
    iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
    ```

5. Проверка:
   - Локальная машина (`ubuntu1`):
     ```bash
     ping google.com  # Пинг должен работать
     ping 172.17.0.2  # Пинг должен быть разрешён
     ```

   - Удалённая машина (`ubuntu2`):
     ```bash
     ping 172.17.0.2  # Пинг не должен работать
     ```

6. Разрешите ping в обоих направлениях: от локальной машины к удаленной машине и наоборот:
    ```bash
    iptables -F
    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    iptables -A OUTPUT -p udp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT  # Разрешить DNS-запросы
    iptables -A INPUT -p udp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A INPUT -p tcp --sport 53 -j ACCEPT   # Разрешить ответы от DNS
    iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
    iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
    iptables -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
    iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
    ```

7. Проверка:
   - Локальная машина (`ubuntu1`):
     ```bash
     ping google.com  # Пинг должен работать
     ping 172.17.0.2  # Пинг должен быть разрешён
     ```

   - Удалённая машина (`ubuntu2`):
     ```bash
     ping 172.17.0.2  # Пинг должен быть разрешён
     ```

### Скриншоты

- ![Задание 6.1](Задание%206.%20DROP%20INPUT%20&%20OUTPUT/Задание_6_1.png)
- ![Задание 6.2](Задание%206.%20DROP%20INPUT%20&%20OUTPUT/Задание_6_2.png)
- ![Задание 6.3](Задание%206.%20DROP%20INPUT%20&%20OUTPUT/Задание_6_3.png)
- ![Задание 6.4](Задание%206.%20DROP%20INPUT%20&%20OUTPUT/Задание_6_4.png)